Feature: JSample

  Scenario: reading values by key
    Given I have a JSample defined by { "name" : "I'm a JSample" }
    Then the json string is { "name" : "I'm a JSample" }
    And the value of name is I'm a JSample

  Scenario: getting an unknown key returns null
    Given I have a JSample defined by { "name" : "I'm a JSample" }
    Then getting unknown key returns null

  Scenario: adding a new key
    Given I have a JSample defined by { "first_name" : "Frank" }
    When I update the sample with { "last_name" : "Zappa" }
    Then the value of first_name is Frank
    And the value of last_name is Zappa

  Scenario: merging with another json string
    Given I have a JSample defined by { "name" : "Hans" }
    When I update the sample with { "name" : "Hansi", "profession" : "Singer" }
    Then the value of name is Hansi
    And the value of profession is Singer

  Scenario: last of a duplicated key wins
    Given I have a JSample defined by { "name" : "Hans" }
    When I update the sample with { "name" : "Hansi", "profession" : "Singer", "name" : "Hons" }
    Then sample as json is {"profession":"Singer","name":"Hons"}
    And the value of profession is Singer

  Scenario: updating a single key
    Given I have a JSample defined by { "id" : 123, "title" : "The Sample" }
    When I update id with 0x1234
    Then the value of id is 0x1234
    And the value of title is The Sample

  Scenario: removing a key
    Given I have a JSample defined by { "id" : "123", "title" : "The Sample" }
    When I remove title
    Then getting title returns null
    And the value of id is 123

  Scenario: The entire JSON-String is an Array
    Given I have a JSample defined by ["1","2","The End"]
    Then the element at position 0 is 1
    And the element at position 1 is 2
    And the element at position 2 is The End

  Scenario: The JSON-String defines an Array of objects
    Given I have a JSample defined by [ { "name" : "Frank" }, { "name" : "Moon" } ]
    Then name of element 0 is Frank
    And name of element 1 is Moon

  Scenario: The JSON-String is an array, containing a mix of values and objects
    Given I have a JSample defined by [ "1", { "name" : "Zappa" } ]
    Then the element at position 0 is 1
    And name of element 1 is Zappa

  Scenario: A JsonParseException is thrown on parse errors
    Given The following string to compile { "x" : 1 : invalid }
    Then I expect a JsonParseException

  Scenario: Accessing an index out of boundaries
    Given The following string to compile [ 1, 2, 3 ]
    Then accessing element out of boundaries throws exception IndexOutOfBoundsException