package cc_application.tests.jsample;

import cc_application.exceptions.JsonParseException;
import cc_application.jsample.Base;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;

import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

@SuppressWarnings("unused")
public class Steps {

    private final static Logger logger = Logger.getLogger(Steps.class.getName());
    private static Base sample = null;
    private static String toCompile = null;

    @Before
    public void setUp() {
        sample = null;
    }

    @Then("^the json string is (.*)$")
    public void theJsonStringIs(String expected) throws Throwable {
        assertEquals(expected, sample.initialJson());
    }

    @Given("^I have a JSample defined by (.*)$")
    public void iHaveAJSampleDefinedByName(String jsonString) throws Throwable {
        sample = new Base(jsonString);
    }

    @Then("^the value of (\\w+) is (.*)$")
    public void theValueOfNameIs(String key, String expected) throws Throwable {
        assertEquals(sample.getKey(key), expected);
    }

    @Then("^get (\\w+) returns null$")
    public void getNotFoundReturnsNull(String key) throws Throwable {
        assertNull(sample.getKey(key));
    }

    @Then("^getting (.*) returns null$")
    public void gettingAnUnknownKeyReturnsNull(String key) throws Throwable {
        Object got = sample.getKey(key);
        assertNull( got );
    }

    @Then("^getting (.*) returns \"(.*)\"$")
    public void gettingNameReturns(String key, String expected) throws Throwable {
        assertEquals(expected, sample.getKey(key));
    }

    @When("^I update the sample with (.*)$")
    public void iUpdateTheSampleWith(String jsonString) throws Throwable {
        sample.merge(jsonString);
    }

    @When("^I update (\\w+) with (.*)$")
    public void iUpdateTheIdWithX(String key, String value) throws Throwable {
        sample.update(key, value);
    }

    @When("^I remove (\\w+)$")
    public void iRemoveKey(Object key) throws Throwable {
        sample.removeKey(key);
    }

    @And("^the element at position (\\d+) is (.*)$")
    public void theElementAtPositionIs(int position, Object expected) throws Throwable {
        assertEquals(expected, sample.at(position));
    }

    @Then("^name of element (\\d+) is (.*)$")
    public void nameOfElementIsFrank(int position, String expected) throws Throwable {
        JSONObject element = (JSONObject) sample.at(position);
        assertEquals(expected, element.get("name"));
    }

    @Given("^The following string to compile (.*)$")
    public void iHaveAnInvalidJsonStringIsInvalid(String jsonString) throws Throwable {
        toCompile = jsonString;
    }

    @Then("^I expect a JsonParseException$")
    public void iExpectAJsonParseException() throws Throwable {
        try {
            new Base(toCompile);
            fail(String.format("Exception JsonParseException expected but not thrown for: %s", toCompile));
        } catch (JsonParseException e) {
            // noop
        }
    }

    @Then("^accessing element out of boundaries throws exception IndexOutOfBoundsException$")
    public void accessingElementOutOfBoundariesThrowsExceptionIndexOutOfBoundsException() throws Throwable {
        try {
            Base entity = new Base(toCompile);
            int size = entity.count();
            entity.at(size);
            fail(String.format("Exception IndexOutOfBoundsException expected but not thrown for: %s", toCompile));
        } catch (IndexOutOfBoundsException _e) {
            // noop
        }
    }

    @And("^there is no field xy$")
    public void thereIsNoFieldXy() throws Throwable{
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^sample as json is (.+)$")
    public void sampleToJsonIs(String json) throws Throwable{
        Assert.assertEquals(sample.toJson(), json);
    }

}
