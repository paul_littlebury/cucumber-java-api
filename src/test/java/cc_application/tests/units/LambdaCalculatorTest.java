package cc_application.tests.units;

/**
 * These tests are to fiddle about but not used within the application.
 */

import org.junit.Test;

import static java.lang.Math.*;
import static org.junit.Assert.*;

/**
 * Ruby example
 *
 * def calculator(*args)
 *   rc = args.first
 *   args[1..-1].each { |a| rc = yield(rc,a) }
 * end
 *
 * calculator(1,1,1,1) { |x,y| x + y } #=>4
 * calculator(10,1,1)  { |x,y| x - y } #=>8
 * calculator(10,2,5)  { |x,y| x * y } #=>100
 * calculator(100,5,2) { |x,y| x / y } #=>10
 *
 */



interface TwoIntCalculator {
    public int calc(int a, int b);
}

class IntCalculator {

    static long calc(TwoIntCalculator calculator, int... args) {
        int rc = args[0];
        for(int i=1; i < args.length; i++) {
            rc = calculator.calc (rc,args[i]);
        }
        return rc;
    }

}

public class LambdaCalculatorTest {
    @Test
    public void operatorAndArgumentsTest() {
        TwoIntCalculator adder = (a, b) -> a + b;
        TwoIntCalculator substractor = (a, b) -> a - b;
        TwoIntCalculator multiplier = (a, b) -> a * b;
        TwoIntCalculator divisor = (a, b) -> a / b;
        TwoIntCalculator power = (a, b) -> (int)round( pow(a,b));

        assertEquals(2, adder.calc(1,1));
        assertEquals(1, substractor.calc(2,1));
        assertEquals(9, multiplier.calc(3,3));
        assertEquals(5, divisor.calc(10,2));

        assertEquals(6, IntCalculator.calc(adder,1,2,3));
        assertEquals(4, IntCalculator.calc(substractor,10,1,2,3));
        assertEquals(6, IntCalculator.calc(multiplier,1,2,3));
        assertEquals(2, IntCalculator.calc(divisor,100,2,5,5));
        assertEquals(65536, IntCalculator.calc(power,2,2,2,2,2));
        assertEquals(65536, IntCalculator.calc(power,2,16));
    }
}


