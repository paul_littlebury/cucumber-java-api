package cc_application.tests.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;
import cc_application.requests.Base;
import cc_application.usecases.Factory;
import cucumber.api.PendingException;
import cucumber.api.Scenario;

import cc_application.support.Config;
import cucumber.api.java8.En;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import static java.lang.Integer.toUnsignedLong;
import static org.junit.Assert.assertEquals;

@SuppressWarnings("ALL")
public class TimeStepdefs implements En {
    private Map<String, Object> responseData;
    private cc_application.responses.Base response;
    private Base request;
    private String respondedTime;
    private String expectedTime = null;

    public TimeStepdefs() {

        Before( () -> {
            Config.log(Level.FINER, "Freeze time");
            Config.freezeTimeAt(ZonedDateTime.now());
        });

        After( (Scenario scenario) -> {
            Config.log(Level.FINER, "Unfreeze time");
            Config.unfreezeTime();
        });

        Given("^the current time$", () -> {
            expectedTime = ZonedDateTime.now(Config.clock()).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        });

        When("^executing the request (.*)$", (String jsonString) -> {
            try {
                request = new cc_application.requests.Base(jsonString);
                cc_application.usecases.Base useCase = Factory.buildUseCase(request);
                response = useCase != null ? useCase.execute() : null;
            } catch (InvalidRequestException | JsonParseException e) {
                e.printStackTrace();
            }
            //noinspection unchecked
            responseData = (HashMap) (response != null ? response.data() : null);
            respondedTime = (String) (responseData != null ? responseData.get("iso") : null);
        });

        Then("^the responded time is the expected time$", () -> {
            assertEquals(expectedTime,respondedTime);
        });

        Then ("^the responded age is (\\d+) days$", (Integer days) -> {
            assertEquals( toUnsignedLong(days), responseData.get( "days" ) );
        });

        Then( "^the responded age is -(\\d+) days$", (Integer days) -> {
            assertEquals( new Long(-1 * days), responseData.get( "days" ) );
        } );
    }
}


