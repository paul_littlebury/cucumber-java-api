package cc_application.exceptions;

/**
 * The exception gets thrown by /server/shutdown request
 */
public class ServerStoppedByRequest extends Throwable {
    public ServerStoppedByRequest(String message) {
       super(message);
    }
}
