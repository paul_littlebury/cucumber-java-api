/**
 * A simple json-request
 *
 *     {
 *       "method" : "get,put,push,delete,info,...",
 *       "path"   : "ping,echo,your use case,null",
 *       "body"   : "any optional content (params)"
 *     }
 */
package cc_application.requests;