package cc_application.requests;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;

import java.util.ArrayList;
import java.util.logging.Level;

import static cc_application.support.Config.log;

/**
 * A Base request is defined by a json-string and must contain the keys
 *
 *   - `path`
 *   - `method`
 *
 * and optionally
 *
 *  - `body`
 *
 * Example:
 *
 *     { "method" : "get", "path" : "ping" }
 */
@SuppressWarnings("CodeBlock2Expr")
public class Base {
    private cc_application.jsample.Base requestModel;
    private ArrayList<String> arguments = null;

    public Base(String requestJson) throws InvalidRequestException {
        try {
            requestModel = new cc_application.jsample.Base(requestJson);
        } catch (JsonParseException e) {
            log(Level.WARNING, String.format("Can't create JSample from '%s'. Error: '%s'", requestJson, e.getMessage()));
            throw new InvalidRequestException(String.format("Invalid JSON '%s' - '%s'",
                    requestJson, e.getLocalizedMessage()));
        }
    }

    @SuppressWarnings("SameReturnValue")
    public boolean isValid() { return true; }

    /**
     * Return the request's path or throw an exception
     * @return the value of path
     * @throws InvalidRequestException when no path is defined
     */
    public String path() throws InvalidRequestException {
        String p = (String) requestModel.getKey("path");
        if (p == null)
            throw new InvalidRequestException("path can't be null");
        return p;
    }

    /**
     * @return the method name (defaults to 'get')
     */
    public String method() {
        Object m = requestModel.getKey("method");
        return m != null ? m.toString() : "GET";
    }

    /**
     * @return value of body or an empty string if body is not present.
     */
    public String body() {
        try {
            return requestModel.getKey("body").toString();
        } catch (NullPointerException e) {
            return "";
        }
    }

    /**
     * @see cc_application.usecases.Factory
     * @return Uppercased path
     * @throws InvalidRequestException when path is not set or not a string
     */
    public String useCaseName() throws InvalidRequestException {
        String[] parts = path().split("\\/");
        String command = firstPart(parts);
        return command.substring(0, 1).toUpperCase() +
               command.substring(1).toLowerCase();
    }

    private String firstPart(String[] parts) {
        for( String part : parts ) {
            if( !part.equals("") ) return part;
        }
        return "";
    }

    public ArrayList<String> arguments() throws InvalidRequestException {
        if (this.arguments == null) setupArguments();
        return this.arguments;
    }

    private void setupArguments() throws InvalidRequestException {
        this.arguments = new ArrayList<>();
        String[] split = path().split("\\/");
        for( String part : split ) {
            if (part != null && !part.equals(""))
                this.arguments.add(part);
        }
    }

}
