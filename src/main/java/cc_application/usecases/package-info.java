/**
 * Package to define use cases
 * @see cc_application.usecases.Base
 */
package cc_application.usecases;