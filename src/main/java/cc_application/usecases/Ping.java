package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;

/**
 * <h3>Usecase Ping</h3>
 *
 * The simplest use case returns pong when receiving ping as a get-request
 *
 * <h4>Request</h4>
 * <pre>
 *     { "method" : "get", "path" : "ping"  }
 * </pre>
 *
 * <h4>Response</h4>
 * <pre>
 *     { "meta" : {}, "body" : "pong" }
 * </pre>
 */
public class Ping extends Base {

    public Ping(cc_application.requests.Base request) throws JsonParseException {
        super(request);
    }

    @Override
    public void run() throws InvalidRequestException {
        setData("PONG");
    }
}
