package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;

/**
 * * <h3>Usecase Null</h3>
 *
 * A Null Use case is being built when the given use case was not defined.
 * It returns "&lt;&lt;NULL&gt;&gt;" as string in the body.
 *
 *
 * <h4>Request</h4>
 * <pre>
 *     { "method" : "unknown", "path" : "whatever"  }
 * </pre>
 *
 * <h4>Response</h4>
 * <pre>
 *     { "meta" : {}, "body" : "&lt;&lt;NULL&gt;&gt;" }
 * </pre>
 */
public class Null extends Base {
    public Null(cc_application.requests.Base request) throws JsonParseException {
        super(request);
    }

    @Override
    public void run() throws InvalidRequestException {
        setData("NULL");
    }
}
