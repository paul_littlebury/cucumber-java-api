package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;
import cc_application.support.Config;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class Age extends Base {
    public Age(cc_application.requests.Base request) throws JsonParseException {
        super(request);
    }

    @Override
    public void run() {
        HashMap<String,String> dates = parseRequest();

        ZonedDateTime date1 = getZonedDateTime(dates.get("date1str"));
        ZonedDateTime date2 = getZonedDateTime(dates.get("date2str"));
        HashMap<String, Object> dmap = buildResponseHash(date1,date2);
        this.setData(dmap);
    }

    private HashMap parseRequest() {
        HashMap dates = new HashMap();
        try {
            if(request.arguments().size() > 2) {
                dates.put ( "date1str", request.arguments().get(1) + "T00:00:00+00:00");
                dates.put ( "date2str", request.arguments().get(2) + "T00:00:00+00:00");
            }
        } catch (InvalidRequestException e) {
            this.addError(0, e.getMessage());
            e.printStackTrace();
        }
        return dates;
    }

    private HashMap<String, Object> buildResponseHash(ZonedDateTime date1, ZonedDateTime date2) {
        HashMap<String, Object> dmap = new HashMap<>();
        Duration duration = Duration.between(date1,date2);
        dmap.put("days", duration.toDays());
        return dmap;
    }

    private ZonedDateTime getZonedDateTime(String reqDateTimeString) {
        ZonedDateTime requestedDate;
        try {
            requestedDate = ZonedDateTime.parse(reqDateTimeString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        } catch (Exception e) {
            addError(0, e.getMessage());
            requestedDate = null;
        }
        return requestedDate;
    }
}
