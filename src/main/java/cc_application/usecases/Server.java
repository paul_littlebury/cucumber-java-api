package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;

public class Server extends Base {
    public Server(cc_application.requests.Base request) throws JsonParseException {
        super(request);
    }

    @Override
    protected void run() throws InvalidRequestException {
        String group = request.arguments().get(0);
        String command = request.arguments().get(1);
        if (!"server".equals(group)) return;

        if ("shutdown".equals(command)) {
            setData("<<STOPPING>>");
        }
    }
}
