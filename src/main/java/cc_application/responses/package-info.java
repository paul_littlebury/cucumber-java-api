/**
 * A simple json-response
 *
 *     {
 *       "meta" : {},
 *       "body"   : "a json-parse-able content"
 *     }
 */
package cc_application.responses;