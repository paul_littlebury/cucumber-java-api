package cc_application.responses;

import cc_application.jsonapi.Meta;

import java.util.ArrayList;
import java.util.Objects;

/**
 * The Response object is a JSON-based entity with meta and a body.
 * The body is a JSON-parse able string.
 * The usecase object is responsible to setup the response object.
 *
 * Example:
 *
 *    { meta: {}, body: "pong" }
 *
 * @see cc_application.usecases
 */
public class Base {
    private final cc_application.jsample.Base responseObject;
    private final Object data;
    private final ArrayList errors;
    private final Meta meta;

    public Base(cc_application.jsample.Base response) {
        responseObject = response;
        data = responseObject.getKey("data");
        meta = new Meta(responseObject.getKey("meta"));
        errors = (ArrayList) responseObject.getKey("errors");
    }

    public String body() {
        return responseObject.toJson();
    }

    public Object data() {
        return this.data;
    }

    public ArrayList errors() {
        return this.errors;
    }

    public String toJson() {
        return body();
    }

    public boolean hasMeta() {
        return meta != null && !Objects.equals(meta.toString(), "");
    }
}
