package cc_application.support;

import java.lang.management.ManagementFactory;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.System.*;

/**
 * The config class provides assets used all over the application and tests.
 *
 *   - Config.log can be used anywhere to output logs throug a single Logger-object provided by Config
 */
@SuppressWarnings("unused")
public class Config {

    // Singleton
    private static final Config ourInstance = new Config();

    /*
     ========================================================================
     Config your application here
     ========================================================================
    */
    private static final String APP_NAME = "jSample";
    private static final int SERVER_PORT = 8808;
    public static final String DEFAULT_CONTENT_TYPE = "text/plain";


    /*
    ========================================================================
    Don't change setup below this line!! (unless you know what you're doing)
    ========================================================================
    */
    private static final String JSON_API_CONTENT_TYPE = "application/vnd.api+json";
    private static final int QUEUE_SIZE = 128;
    private static Logger logger = Logger.getLogger(APP_NAME);
    private static Clock applicationClock;
    private static Clock unfrozenClock;

    /**
     * Constructor sets logger format to single line and adds hostname and pid
     */
    public Config() {
        setLogFormat();
        setLogLevel();
    }

    // Public API

    public static void log(Level level, String message) {
        logger().log(level,message);
    }


    /**
     * Get PID independent from system
     * @return "PID@hostname"
     */
    private String getPidAtHost() {
        return ManagementFactory.getRuntimeMXBean().getName();
    }

    @SuppressWarnings("SameReturnValue")
    private static Config getInstance() {
        return ourInstance;
    }

    // Private

    private static Logger logger() {
        return logger;
    }

    public static int serverPort() {
        return SERVER_PORT;
    }

    /**
     *  @see <a href="http://jsonapi.org/#mime-types">JSON Api</a>
     */
    public static String contentType(String format) {
        switch (format) {
            case "json": return  JSON_API_CONTENT_TYPE;
            default: return DEFAULT_CONTENT_TYPE;
        }
    }

    public static Clock clock() {
        if( applicationClock != null)
            return applicationClock;
        else
            return Clock.systemDefaultZone();
    }

    public static void freezeTimeAt(ZonedDateTime now) {
        unfrozenClock = Clock.systemDefaultZone();
        applicationClock = Clock.fixed(Instant.from(now), ZoneId.systemDefault());
    }

    public static void unfreezeTime() {
        applicationClock = unfrozenClock;
    }

    private void setLogFormat() {
        setProperty("java.util.logging.SimpleFormatter.format",
                String.format("%%1$tF %%1$tT %%4$s [%s,%s] %%5$s %%6$s%%n", APP_NAME, getPidAtHost()));
    }

    private void setLogLevel() {
        String envEnv = System.getenv("CCA_LOG_LEVEL");
        String env = envEnv == null ? "INFO" : envEnv;

        logger = Logger.getLogger(APP_NAME);
        switch (env) {
            case "WARN": logger().setLevel(Level.WARNING); break;
            case "ERROR": logger().setLevel(Level.FINE); break;
            case "FATAL": logger().setLevel(Level.FINER); break;
            case "ALL": logger().setLevel(Level.FINEST); break;
            default: logger().setLevel(Level.INFO);
        }
    }

    public static int queueSize(){
        return QUEUE_SIZE;
    }
}
