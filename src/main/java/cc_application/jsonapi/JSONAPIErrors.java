package cc_application.jsonapi;

import java.util.ArrayList;

@SuppressWarnings("MethodNameSameAsClassName")
public class JSONAPIErrors {

    private ArrayList<Error> errors;

    public void add(int status, String title) {
        if (errors == null)
          errors = new ArrayList<>();
        errors.add(new Error(status, title));
    }

    public JSONAPIErrors() {
        errors = null;
    }

    public ArrayList<Error> errors() {
        return this.errors;
    }
}
