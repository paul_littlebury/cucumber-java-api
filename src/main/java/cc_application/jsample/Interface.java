package cc_application.jsample;

import cc_application.exceptions.JsonParseException;

/**
 * Guarantee a set of methods we can expect for any entity, represented by
 * a JSon object. The implementation of `Base` is supposed to
 * fulfil the actions using any json-library.
 * <p>
 *     Our implementation can handle Lists [] and Maps {} <br/>
 *     Anyhow, setting up a Map and then call list-functions on it will fail.
 * </p>
 */
@SuppressWarnings("unused")
interface Interface {
    /**
     * @return The originaly passed json-string - not modified after construction.
     */
    String initialJson();

    /**
     * Find the object of key `key` within a Map
     * @param key the key to search for
     * @return Any object found or null
     */
    Object getKey(Object key);

    /**
     * Merge the current object (if it's a Map) with the given
     * Map defined by `jsonString`.
     * @param jsonString a valid json-string
     * @throws JsonParseException when jsonString can't be parsed
     */
    void merge(String jsonString) throws JsonParseException;

    /**
     * Update the current Map. If `key` is not found, a new entry will be added.
     * @param key The key to update or add
     * @param value The value of the key
     */
    void update(Object key, Object value);

    /**
     * Remove the given `key` from the Map.
     * Do nothing if key doesn't exist.
     * @param key The key to be removed
     */
    void removeKey(Object key);

    /**
     * Return the object at position `idx`
     * @param idx Zero based position to be fetched
     * @return Any object
     * @throws IndexOutOfBoundsException when accessing an index out of bounds.
     */
    Object at(int idx);

    /**
     * @return The json-representation of the current state (object)
     */
    String toJson();

    int count();
}
